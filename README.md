# APP F&I - DataMuffin's Proposal
## Descripción
No es algo desconocido como ha crecido el mercado del desarrollo de software durante el siglo XXI, cada vez son más las empresas que se apuntan a la digitalización y al negocio del mercado digital.

La importancia de hacer llegar tu modelo de negocio a los consumidores es cada vez mas alta, y en un mundo donde el Smartphone está tan presente ([¡2021 el 97% de los hogares!](https://es.statista.com/estadisticas/718350/porcentaje-de-viviendas-con-telefono-movil-espana/#:~:text=El%20porcentaje%20de%20hogares%20dotados%20de%20uno%20o,telefon%C3%ADa%20m%C3%B3vil%2C%20frente%20al%2090%2C7%25%20registrado%20en%202011.)) aparece **the DataMuffin's Proposal**:

[<center><img src="https://gitkc.cloud/data-muffin/proyecto-final/-/raw/main/Presentaci%C3%B3n/Logo.png" alt="drawing" width="300"/></center>](https://gitkc.cloud/data-muffin/proyecto-final/-/tree/main)


# App F&I
## ¿Que es APP F&I?
**App F&I** es una solución propuesta por **DataMuffin** para inversores y desarrolladores, donde se utiliza tecnologías propias del campo de la **IA**, **Big Data** y **Machine Learning** para lograr el máximo índice de éxito mediante un análisis de mercado propio de cualquier *App* y en cualquier mercado especificado.

**App F&I** análiza las fortalezas y debilidades de una aplicación para poder focalizar y mejorar los puntos mas interesantes, con el objetivo de lograr el máximo beneficio posible.

# Instrucciones
Existen 2 métodos de replicar la total instalación de la infractuctura y desarrollo de esta aplicación, mediante Docker o mediante Poetry, pero, al ser un proyecto basado en cloud se necesita varios requisitos.

Todos los requisitos y las instrucciones al detalles se encuentran en las carpetas de cada directorio Docker (en orden del 1 al 4) dentro de este repositorio.

# Nota
Este es un proyecto para Keepcoding

[<center><img src="https://keepcoding.io/wp-content/uploads/2022/01/cropped-logo-keepcoding-Tech-School.png" alt="drawing" width="300"/></center>](https://keepcoding.io/)
