import os
import pickle
from google.cloud import storage

def save_dict_smooth_into_bucket(dict_smooth, project, bucket_name):

  # Credenciales
  os.environ [ 'GOOGLE_APPLICATION_CREDENTIALS'] = './resources/credentials.json'

  # bucket
  storage_client = storage.Client()
  bucket = storage_client.bucket(bucket_name)

  # Blob
  blob = bucket.blob('Preprocess/dict_smooth.pkl')
  pickle_out = pickle.dumps(dict_smooth)
  blob.upload_from_string(pickle_out)

def save_models_to_bucket(encoder_permission, encoder_temp, project, bucket_name):

  # Credenciales
  os.environ [ 'GOOGLE_APPLICATION_CREDENTIALS'] = './resources/credentials.json'

  # bucket
  storage_client = storage.Client()
  bucket = storage_client.bucket(bucket_name)

  # Blob
  blob = bucket.blob('Preprocess/encoder_permission.pkl')
  pickle_out = pickle.dumps(encoder_permission)
  blob.upload_from_string(pickle_out)

  blob = bucket.blob('Preprocess/encoder_temp.pkl')
  pickle_out = pickle.dumps(encoder_temp)
  blob.upload_from_string(pickle_out)