# Base de datos
import pandas as pd
import psycopg2
from sqlalchemy import create_engine

# Conexión a la base de datos
def connect_to_database(database, user, password, host, port, sqlalchemy=False):
  if sqlalchemy:
    conn_string = 'postgresql://{}:{}@{}/{}'.format(
        user, 
        password, 
        host, 
        database)
    db = create_engine(conn_string)
    conn_alchemy = db.connect()
    return conn_alchemy

  else:
    try:
      conn=psycopg2.connect(
          database=database,
          user=user,
          password=password,
          host=host,
          port="{}".format(port))
      return conn
    except:
      print("Error: Conexión con la base de datos no establecida")

# Cerramos conexión
def close_connection_to_database(conn, save=True):
  try:
    if save:
      conn.commit()
    conn.close()
  except:
    print('Warning: Problema al cerrar conexión, posiblemente ya cerrada')
    pass

def read_data_mart(conn_alchemy):
  return pd.read_sql("SELECT * FROM data_mart", conn_alchemy, index_col='id_app_list')

def upload_to_database(conn_alchemy, df):
  df.to_sql('data_mart', conn_alchemy, if_exists= 'replace', method='multi')

# Nombre de tablas
def get_tables_name():
    names = [
        'app_list', 'info_app', 'permissions_app', 'image_app',
        'date_release_scrape', 'calculated_fields']
    return names

# Devuelve tabla
def get_table(conn_alchemy, table_name):
    if table_name in get_tables_name():
        return pd.read_sql("SELECT * FROM {}".format(table_name), conn_alchemy)
    else:
        return None
