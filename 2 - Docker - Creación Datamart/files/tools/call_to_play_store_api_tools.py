from google_play_scraper import reviews_all, app
from tqdm import tqdm
import math
import datetime
import numpy as np
import pandas as pd
import pickle
import signal
from os.path import exists
from os import remove

# Timeout
def handler(signum, frame):
  raise Exception("end of time")

# --------------------------------- call to api app -------------------------------------
# Datos de competencia a partir de una id
def get_competency_data_from_id(df, id, language, country):
    # Time out
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(10)
    # Comprobar si está en el dataframe
    if id in df['id_app'].values:
        return [df[df['id_app'] == id][col].values[0] for col in ['min_installs', 'score', 'reviews']]
    # Comprobar si existe checkpoint
    res_dict = dict()
    if exists("checkpoint1.pkl"):
        with open('checkpoint1.pkl', 'rb') as f:
            res_dict = pickle.load(f)
    # Comprobar si está en el checkpoint
    if id in res_dict:
        return res_dict[id]
    # Ultima opción, llamar a la api y guardar checkpoint
    try:
        res = app(id, lang = language, country=country)
        res_dict[id] = [res['minInstalls'] , res['score'], res['reviews']]
        with open('checkpoint1.pkl', 'wb') as f:
            pickle.dump(res_dict, f)
    except:
        return None
        
# Datos de competencia a partir de una lista de ids
def get_competency_data_from_list(df, ids, language, country):
    try:
        return tuple(np.mean([get_competency_data_from_id(df, id, language, country) for id in ids], axis=0))
    except:
        return [np.nan, np.nan, np.nan]

# Datos de competencia a partir de df
def get_competency_data(df, df_backup, language, country):
    if exists("df_1.pkl"):
        with open('df_1.pkl', 'rb') as f:
            return pickle.load(f)

    # Proceso
    tqdm.pandas(desc="get_competency_data")
    df.loc[:,'similar_apps'] = df.loc[:, 'similar_apps'].progress_apply(lambda similars: get_competency_data_from_list(df_backup, similars, language, country))
    df['mean_min_installs_competency'] = df.loc[:, 'similar_apps'].apply(lambda x: math.log10(x[0]+1))
    df['mean_score_competency'] = df.loc[:, 'similar_apps'].apply(lambda x: x[1])
    df['mean_reviews_competency'] = df.loc[:, 'similar_apps'].apply(lambda x: math.log10(x[2]+1))
    signal.alarm(0)
    return df.dropna().drop('similar_apps', axis=1)  

# --------------------------------- call to api reviews_all -------------------------------------
def call_to_api_reviews_all(id_app, language, country, months=24):
    # Comprobamos si existe checkpoint
    res_dict = dict()
    if exists("checkpoint2.pkl"):
        with open('checkpoint2.pkl', 'rb') as f:
            res_dict = pickle.load(f)
    # Comprobar si está en el checkpoint
    if id_app in res_dict:
        return res_dict[id_app]
    # Llamada a la api
    data_reviews = reviews_all(id_app, lang = language, country = country)
    # Iniciamos diccionario
    res = dict()
    # Iniciamos variables
    res['rateThumbsUpCountByReview'] = 0
    res['rateReplyByReviews'] = 0
    res['MeanScoreComment'] = 0
    # Creando columnas past_month
    for i in range(24):
        res['past_months_{}'.format(i)] = 0
    # Caso 1 - Tenemos reviews
    if len(data_reviews) > 0:
        # Fecha de hoy
        today = datetime.datetime.now()
        # Convierto a dataframe
        data_reviews = pd.DataFrame(data_reviews)
        data_reviews['delta'] = today - data_reviews['at']
        data_reviews['months'] = data_reviews['delta']/np.timedelta64(1, 'M')
        data_reviews[data_reviews['months'] <= months]
        data_reviews['months'] = data_reviews['months'].astype(int)
        # Creando columnas past_month
        for i in range(24):
            reviews_on_month = data_reviews[data_reviews['months'] == i].shape[0]
            total = data_reviews.shape[0]
            res['past_months_{}'.format(i)] = reviews_on_month / total
        # Resto de variables
        res['rate_thumbsupcount_by_review'] = data_reviews['thumbsUpCount'].sum()/data_reviews.shape[0]
        res['rate_reply_by_reviews'] = data_reviews[data_reviews['replyContent'].notnull()].shape[0]/data_reviews.shape[0]
        res['mean_score_comment'] = data_reviews['score'].mean()
        res_dict[id_app] = res
        with open('checkpoint2.pkl', 'wb') as f:
            pickle.dump(res_dict, f)
    return res

def get_reviews_data(df, language, country):
    if exists("df_2.pkl"):
        with open('df_2.pkl', 'rb') as f:
            return pickle.load(f)

    id_apps = tqdm(df['id_app'].values)
    for id_app in id_apps:
        id_apps.set_description("get_reviews_data")
        try:
            res = call_to_api_reviews_all(id_app, language=language, country=country)
            df.loc[df['id_app'] == id_app, 'rate_thumbsupcount_by_review'] = res['rate_thumbsupcount_by_review']
            df.loc[df['id_app'] == id_app, 'rate_reply_by_reviews'] = res['rate_reply_by_reviews']
            df.loc[df['id_app'] == id_app, 'mean_score_comment'] = res['mean_score_comment']
            for i in range(24):
                df.loc[df['id_app'] == id_app, 'past_months_{}'.format(i)] = res['past_months_{}'.format(i)]
        except:
            df.loc[df['id_app'] == id_app, 'rate_thumbsupcount_by_review'] = np.nan
            df.loc[df['id_app'] == id_app, 'rate_reply_by_reviews'] = np.nan
            df.loc[df['id_app'] == id_app, 'mean_score_comment'] = np.nan
            for i in range(24):
                df.loc[df['id_app'] == id_app, 'past_months_{}'.format(i)] = np.nan
    return df.dropna()