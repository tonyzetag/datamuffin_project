BEGIN;


CREATE TABLE IF NOT EXISTS public.app_list
(
    id integer NOT NULL DEFAULT nextval('app_list_id_seq'::regclass),
    id_app text COLLATE pg_catalog."default" NOT NULL,
    status smallint,
    CONSTRAINT app_list_pkey PRIMARY KEY (id),
    CONSTRAINT app_list_id_app_key UNIQUE (id_app)
);

CREATE TABLE IF NOT EXISTS public.calculated_fields
(
    id integer NOT NULL DEFAULT nextval('calculated_fields_id_seq'::regclass),
    id_app_list integer,
    ratings integer,
    dif_histogram integer,
    days_since_released integer,
    months_since_released integer,
    ever_updated boolean,
    CONSTRAINT calculated_fields_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.data_mart
(
    id integer NOT NULL,
    title double precision,
    content_rating double precision,
    months_since_released double precision,
    in_app_product_price double precision,
    n_screenshots double precision,
    dif_histogram double precision,
    size_app double precision,
    description double precision,
    ratings double precision,
    genre_id double precision,
    min_installs double precision,
    score double precision,
    date_release double precision,
    video double precision,
    id_app_list integer,
    developer_id double precision,
    days_since_released double precision,
    android_version double precision,
    reviews double precision,
    summary double precision,
    ever_updated double precision,
    id_app text COLLATE pg_catalog."default",
    released_year double precision,
    released_month double precision,
    released_day double precision,
    mean_min_installs_competency double precision,
    mean_score_competency double precision,
    mean_reviews_competency double precision,
    rate_thumbsupcount_by_review double precision,
    rate_reply_by_reviews double precision,
    mean_score_comment double precision,
    permission double precision,
    temp double precision,
    install_label bigint,
    CONSTRAINT data_mart_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.date_release_scrape
(
    id integer NOT NULL DEFAULT nextval('date_realease_scrape_id_seq'::regclass),
    id_app_list integer,
    date_release date,
    date_scrape date NOT NULL,
    CONSTRAINT date_realease_scrape_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.extracted_images_app
(
    id_app_list integer,
    image_icon bytea,
    image_header bytea,
    image_is_truncated boolean,
    id integer NOT NULL DEFAULT nextval('extracted_images_app_id_seq1'::regclass),
    CONSTRAINT extracted_images_app_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.image_app
(
    id integer NOT NULL DEFAULT nextval('image_app_id_seq'::regclass),
    id_app_list integer,
    n_screenshots integer,
    icon_url text COLLATE pg_catalog."default",
    header_url text COLLATE pg_catalog."default",
    CONSTRAINT image_app_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.info_app
(
    id integer NOT NULL DEFAULT nextval('info_app_id_seq'::regclass),
    min_installs integer,
    reviews integer,
    free_app boolean,
    offersiap boolean,
    ad_supported boolean,
    id_app_list integer,
    title text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    summary text COLLATE pg_catalog."default",
    score real,
    price real,
    in_app_product_price text COLLATE pg_catalog."default",
    size_app real,
    android_version text COLLATE pg_catalog."default",
    developer text COLLATE pg_catalog."default",
    developer_id text COLLATE pg_catalog."default",
    genre_id text COLLATE pg_catalog."default",
    video integer,
    content_rating text COLLATE pg_catalog."default",
    contains_ad boolean,
    similar_apps text[] COLLATE pg_catalog."default",
    CONSTRAINT info_app_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.permissions_app
(
    id integer NOT NULL DEFAULT nextval('permissions_app_id_seq'::regclass),
    id_app_list integer,
    microphone integer,
    wifi_connect_info integer,
    camera integer,
    photos_media_files integer,
    storage_per integer,
    device_id_call_info integer,
    contacts integer,
    phone integer,
    identity_per integer,
    other integer,
    uncategorized integer,
    device_and_app_history integer,
    location integer,
    cellular_data_setting integer,
    calendar integer,
    CONSTRAINT permissions_app_pkey PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS public.calculated_fields
    ADD CONSTRAINT calculated_fields_id_app_list_fkey FOREIGN KEY (id_app_list)
    REFERENCES public.app_list (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;


ALTER TABLE IF EXISTS public.data_mart
    ADD CONSTRAINT id_app_list FOREIGN KEY (id_app_list)
    REFERENCES public.app_list (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;


ALTER TABLE IF EXISTS public.date_release_scrape
    ADD CONSTRAINT date_realease_scrape_id_app_list_fkey FOREIGN KEY (id_app_list)
    REFERENCES public.app_list (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;


ALTER TABLE IF EXISTS public.extracted_images_app
    ADD CONSTRAINT id_app_list FOREIGN KEY (id_app_list)
    REFERENCES public.app_list (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;


ALTER TABLE IF EXISTS public.image_app
    ADD CONSTRAINT image_app_id_app_list_fkey FOREIGN KEY (id_app_list)
    REFERENCES public.app_list (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;


ALTER TABLE IF EXISTS public.info_app
    ADD CONSTRAINT id_app_list FOREIGN KEY (id_app_list)
    REFERENCES public.app_list (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

END;
