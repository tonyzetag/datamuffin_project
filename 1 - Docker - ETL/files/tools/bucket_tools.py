import os
import pandas as pd

BUCKET_NAME = "storage-play-store"
GCP_BUCKET = 'gs://' + BUCKET_NAME
google_playstore_csv_path = GCP_BUCKET + '/Datalake/Kaggle/Google-Playstore.csv'

def get_id_apps_from_kaggle_file(google_playstore_csv_path, gcp_credentials = './resources/credentials.json'):
  """
  Devuelve las Ids de aplicaciones del archivo de Kaggle en formato set()
  """
  # Credenciales
  os.environ [ 'GOOGLE_APPLICATION_CREDENTIALS'] = gcp_credentials

  # Definiendo categorías válidas
  valid_categories = {'Action', 'Adventure', 'Arcade', 'Board', 'Card', 'Casino', 
                    'Casual', 'Educational', 'Music', 'Puzzle', 'Racing', 'Role Playing', 
                    'Simulation', 'Sports', 'Strategy', 'Trivia', 'Word'}

  # Carga del archivo
  try:
    google_playstore_csv = pd.read_csv(google_playstore_csv_path, sep = ",")[['App Id', 'Category']]
    google_playstore_csv = google_playstore_csv.loc[google_playstore_csv.Category.isin(valid_categories)]
    print("Cargado {} ids desde el archivod e Kaggle".format(len(google_playstore_csv)))
    return set(google_playstore_csv['App Id'])
  except:
    google_playstore_csv = pd.DataFrame()
    return set()

"""
ids = get_id_apps_from_kaggle_file(
  google_playstore_csv_path, 
  './resources/datamuffin-playstore-345816-22feda132c0f.json')
"""
