from google_play_scraper import Sort, reviews, reviews_all, app, permissions

# Columnas válidas
columns =  ['appId', 'title', 'description', 'descriptionHTML', 'summary', 'summaryHTML',
            'installs', 'minInstalls', 'score', 'ratings', 'reviews', 'histogram',
            'price', 'free', 'currency', 'sale', 'saleTime', 'originalPrice',
            'saleText', 'offersIAP', 'inAppProductPrice', 'size', 'androidVersion',
            'androidVersionText', 'developer', 'developerId', 'developerEmail',
            'developerWebsite', 'developerAddress', 'privacyPolicy',
            'developerInternalID', 'genre', 'genreId', 'icon', 'headerImage',
            'screenshots', 'video', 'videoImage', 'contentRating',
            'contentRatingDescription', 'adSupported', 'containsAds', 'released',
            'updated', 'version', 'recentChanges', 'recentChangesHTML', 'comments',
            'editorsChoice', 'similarApps', 'moreByDeveloper', 'url']
unwanted_columns = ['comments', 'descriptionHTML', 'summaryHTML', 'installs', 'sale', 
                    'saleText', 'saleTime', 'developerEmail', 'developerWebsite', 
                    'developerAddress', 'privacyPolicy', 'developerInternalID', 
                    'videoImage', 'version', 'recentChanges', 'recentChangesHTML', 
                    'moreByDeveloper', 'ratings', 'originalPrice', 'contentRatingDescription',
                    'currency', 'genre', 'url', 'editorsChoice', 'androidVersionText']
COLUMNS_APPS = [column for column in columns if column not in unwanted_columns]
PERMISSIONS_COLUMNS = ['Other', 'id_app', 'Photos/Media/Files', 'Storage',
       'Wi-Fi connection information', 'Uncategorized', 'Device & app history',
       'Identity', 'Contacts', 'Location', 'Phone', 'Cellular data settings',
       'Device ID & call information', 'Camera', 'Microphone', 'Calendar']
REVIEWS_COLUMNS = ['id_app', 'rateThumbsUpCountByReview', 'rateReplyByReviews', 'MeanScoreComment']

# Llamadas a la api 
def call_to_api_app(id_app, lang = 'EN', country = 'UK'):
    result = app(
        id_app, 
        lang=lang, 
        country=country)
    result = {key:value for key, value in result.items() if key in COLUMNS_APPS}
    return result

def call_to_api_permissions(id_app, lang = 'EN', country = 'UK'):
    pers = permissions(
        id_app,
        lang=lang,
        country=country)
    pers['id_app'] = id_app
    return pers

def call_to_api(id_app, lang = 'EN', country = 'UK'):
    result = dict()
    # app
    try:
        result['app'] = call_to_api_app(id_app, lang=lang, country=country)
    except:
        return None
    # permissions
    try:
        result['permissions'] = call_to_api_permissions(id_app, lang=lang, country=country)
    except:
        return None
    return result