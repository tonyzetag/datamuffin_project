#Importamos las librerías
import pandas as pd
import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from dateutil.relativedelta import relativedelta
import tensorflow as tf
import json
from sklearn.preprocessing import  MinMaxScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
import math
import imageio as io
from PIL import Image
import os
import gc
import platform
import concurrent
import pickle as pickle
import psycopg2
import joblib
from sqlalchemy import create_engine
from sklearn.metrics import classification_report

from tools.database_tools import *
from tools.bucket_tools import *


# ----------------------- transformaciones ---------------------------
def scale_data(x_train, x_test, scaler):
    """
    Standard Scale test and train data
    """
    scaler = scaler
    x_train_scaled = pd.DataFrame(
        scaler.fit_transform(x_train),
        columns=x_train.columns
    )
    x_test_scaled = pd.DataFrame(
        scaler.transform(x_test),
        columns = x_test.columns
    )

    return x_train_scaled, x_test_scaled, scaler

# ------------------------ Random Forest -----------------------------
def main_RandomForestClasiffiers(X_train, y_train):
    
    X_train, X_val, y_train, y_val =train_test_split(X_train, y_train,test_size = 0.2, random_state=42, shuffle = True, stratify=y_train)

    # Number of trees in random forest
    n_estimators = [int(x) for x in np.linspace(start = 200, stop = 1000, num = 10)]
    # Number of features to consider at every split
    max_features = ['auto', 'sqrt', 'log2']
    # Maximum number of levels in tree
    max_depth = [int(x) for x in np.linspace(10, 110, num = 10)]
    max_depth.append(None)
    # Minimum number of samples required to split a node
    min_samples_split = [2, 5, 10]
    # Minimum number of samples required at each leaf node
    min_samples_leaf = [2, 4]
    # Method of selecting samples for training each tree
    criterion = ['gini', 'entropy']


    parameter_space = {'n_estimators': n_estimators,
               'max_features': max_features,
               'criterion': criterion,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf}

    print("Training Random Forest ...", end=" ")
    rf = RandomForestClassifier()
    rf = GridSearchCV(rf, parameter_space, n_jobs=-1, cv=3)
    rf.fit(X_train, y_train.ravel())
    print("Done")

    # Best paramete set
    print('Best parameters found:\n', rf.best_params_)

    y_true, y_pred = y_val.ravel() , rf.predict(X_val)

    
    print('Results on the test set:')
    print(classification_report(y_true, y_pred))

    joblib.dump(rf, 'model.pkl')

    return rf, rf.best_params_

# ============================== Config ==============================
# Lee la configuración a partir de archivo
def get_db_config():
  config_path = './resources/db_config.json'
  # Leo los datos
  with open(config_path) as f:
    data = f.read()
  # Formateo y devuelvo
  return json.loads(data)

# Lee la configuración a partir de archivo
def get_ex_config():
  config_path = './resources/ex_config.json'
  # Leo los datos
  with open(config_path) as f:
    data = f.read()
  # Formateo y devuelvo
  return json.loads(data)

# Lee la configuración a partir de archivo
def get_gcp_config():
  config_path = './resources/gcp_config.json'
  # Leo los datos
  with open(config_path) as f:
    data = f.read()
  # Formateo y devuelvo
  return json.loads(data)

# -------------------------- Pipeline -------------------------------- 
# Conecto a la base de datos
conn_alchemy = connect_to_database(**get_db_config())
conn = conn_alchemy.connect().execution_options(stream_results=True)

# Descargo la tabla data_mart
df = pd.read_sql("SELECT * FROM data_mart", conn_alchemy, index_col='id_app_list')

df = df.iloc[2:]

df.drop(['min_installs', 'id_app', 'id', 'months_since_released'], axis = 1, inplace = True)

data_0 = df[df['install_label']==0]
data_1 = df[df['install_label']==1]
data_2 = df[df['install_label']==2]

if data_0.shape[0] <= data_1.shape[0] and data_0.shape[0] <= data_2.shape[0]:
    data_1 = data_1.sample(data_0.shape[0], random_state=42)
    data_2 = data_2.sample(data_0.shape[0] ,random_state=42)
    df = pd.concat([data_0, data_1, data_2], axis=0)
elif data_1.shape[0] <= data_0.shape[0] and data_1.shape[0] <= data_2.shape[0]:
    data_0 = data_0.sample(data_1.shape[0], random_state=42)
    data_2 = data_2.sample(data_1.shape[0] ,random_state=42)
    df = pd.concat([data_0, data_1, data_2], axis=0)
else:
    data_1 = data_1.sample(data_2.shape[0], random_state=42)
    data_0 = data_0.sample(data_2.shape[0] ,random_state=42)
    df = pd.concat([data_0, data_1, data_2], axis=0)

X = df[df.columns.drop('install_label')]
y = df[['install_label']]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state=42, shuffle = True, stratify=y)


X_train, X_test, scaler = scale_data(X_train, X_test, MinMaxScaler())
save_to_bucket(scaler, "Preprocess/scaler.save", **get_gcp_config())

X_train = X_train.values
X_test=X_test.values

y_train = y_train.values
y_test = y_test.values

rf_final, params_rf = main_RandomForestClasiffiers(X_train, y_train)
save_to_bucket(rf_final, "Model/rf_final.pkl", **get_gcp_config())
