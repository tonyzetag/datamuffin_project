import os
import joblib
from google.cloud import storage


def save_to_bucket(file, filepath, bucket_name, **kwargs):

  # Credenciales
  os.environ [ 'GOOGLE_APPLICATION_CREDENTIALS'] = './resources/credentials.json'

  # bucket
  storage_client = storage.Client()
  bucket = storage_client.bucket(bucket_name)

  # file
  joblib.dump(file, os.path.basename(filepath))

  # Blob
  blob = bucket.blob('{}'.format(filepath))
  blob.upload_from_filename(os.path.basename(filepath))

  os.remove(os.path.basename(filepath))
