import os
from sqlalchemy import create_engine

def connect_to_database(database, user, password, host, port):
    conn_string = 'postgresql://{}:{}@{}/{}'.format(
        user, 
        password, 
        host, 
        database)
    db = create_engine(conn_string)
    conn_alchemy = db.connect()
    return conn_alchemy