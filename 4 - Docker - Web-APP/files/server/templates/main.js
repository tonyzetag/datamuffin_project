function getVal() {
    const val = document.getElementById('idd_app').value;
    setName(val)
    prediccion(0)
}

function setName(nombre){
    document.getElementById('name_app').innerHTML=nombre
}

function prediccion(predict){

    fetch("./prueba.json")
    .then(response => response.json())
    .then(json => console.log(json));

    if(predict == 0){
        document.getElementById("exito").style.backgroundColor="red"
        document.getElementById('exito').innerHTML="Fracaso"
    }
    else if (predict == 1){
        document.getElementById("exito").style.backgroundColor="yellow"
        document.getElementById('exito').innerHTML="Neutro"
    }
    else{
        document.getElementById("exito").style.backgroundColor="green"
        document.getElementById('exito').innerHTML="Exito"
    }
}
