from flask import Flask, redirect, url_for, request, render_template
import requests
from predict.predict import get_values 
import resources
import os
import socket
import requests
import json

def get():
    endpoint = 'https://ipinfo.io/json'
    response = requests.get(endpoint, verify = True)

    if response.status_code != 200:
        return 'Status:', response.status_code, 'Problem with the request. Exiting.'
        exit()

    data = response.json()

    return data['ip']


app = Flask(__name__, template_folder='templates', static_folder='templates')
"""
with app.app_context():
    context = {'id_app': ' '}
    rendered = render_template('main.html', **context)
"""

# Página principal
@app.route('/')
def home():
    return render_template('main.html', ip=get())

# Acción POST
@app.route('/', methods=['POST'])
def home_post():
    id_app = request.form['set_id']
    return redirect(url_for('predict', id_app=id_app, exito=None, color=None))